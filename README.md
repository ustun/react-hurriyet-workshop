# Yüklenmesi gereken araçlar


1. node.js'in yakın tarihli bir versiyonu, en az 5, mümkünse son versiyonu.

`node --version` çıktısı 5'ten büyük olmalı.

`npm -- version` çıktısı 3'ten büyük olmalı.

Node'un son sürümü http://nodejs.org adresinden indirilebilir.

2. (Tercihen) Webstorm ya da Sublime Text ve Babel Sublime eklentisi (https://github.com/babel/babel-sublime)


# İndirilmesi gereken depo ve kütüphaneler

https://gitlab.com/ustun/react-hurriyet-workshop

adresindeki depo git ile indirilmeli.

git clone https://gitlab.com/ustun/react-hurriyet-workshop

Daha sonra:
```
cd react-hurriyet-workshop
npm install
```
komutları ile gereken kütüphaneler indirilmeli.

Daha sonra:
 ```
npm start
```
ile sunucunun çalıştığından emin olunmalı.

Herhangi bir tarayıcıdan http://localhost:3000 adresine gidildiğinde "Merhaba Dünya" yazısının görüldüğünden emin olunmalı.
