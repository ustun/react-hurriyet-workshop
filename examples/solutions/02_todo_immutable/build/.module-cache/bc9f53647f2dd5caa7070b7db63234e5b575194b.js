
var Todo = React.createClass({displayName: "Todo",

    render: function () {
        return React.createElement("li", null, 
              React.createElement("div", {className: "view"}, 
                React.createElement("input", {className: "toggle", type: "checkbox", checked: this.props.item.completed}), 
                React.createElement("label", null, this.props.item.name), 
                React.createElement("button", {className: "destroy"})
              ), 
              React.createElement("input", {className: "edit", defaultValue: "Buy tomatos"})
            );

    }

});

var Todos = React.createClass({displayName: "Todos",

    render: function () {
        return      React.createElement("ul", {id: "todo-list"}, 
        this.props.items.map(function (item) {
            return React.createElement(Todo, {item: item});
        })
        );
    }

});


var Footer = React.createClass({displayName: "Footer",

    render: function () {
        return React.createElement("footer", {id: "footer", style: {display: 'block'}}, 
          React.createElement("span", {id: "todo-count"}, React.createElement("strong", null, "5"), " items left"), 
          React.createElement("ul", {id: "filters"}, 
            React.createElement("li", null, 
              React.createElement("a", {className: "selected", href: "#/"}, "All")
            ), 
            React.createElement("li", null, 
              React.createElement("a", {href: "#/active"}, "Active")
            ), 
            React.createElement("li", null, 
              React.createElement("a", {href: "#/completed"}, "Completed")
            )
          )
        )
    }

});


var App = React.createClass({displayName: "App",

    getInitialState: function () {
        return {items: [{name: "Do this", completed: true},
                        {name: "Do that", completed: false},
                        {name: "Buy this", completed: false},
                        {name: "Buy that", completed: false},
                        {name: "go to shopping", completed: true}]};
    },
    render: function() {

        var items = this.state.items.slice().sort().reverse();

    return (

      React.createElement("section", {id: "todoapp"}, 
        React.createElement("header", {id: "header"}, 
          React.createElement("h1", null, "todos"), 
          React.createElement("input", {id: "new-todo", placeholder: "What needs to be done?", autofocus: true})
        ), 
        React.createElement("section", {id: "main", style: {display: 'block'}}, 
          React.createElement("input", {id: "toggle-all", type: "checkbox"}), 
          React.createElement("label", {htmlFor: "toggle-all"}, "Mark all as complete"), 
        React.createElement(Todos, {items: items})
        ), 

        React.createElement(Footer, null)
      )
    );
  }
});


React.render(React.createElement(App, null), document.getElementById('app'));
