var FILTER_NAME = "nofilter"
var CLIENT_ID = "8ed8f14f61b24c95ad54583110ace715"
var ENDPOINT = `https://api.instagram.com/v1/tags/${FILTER_NAME}/media/recent?client_id=${CLIENT_ID}`
var Instagram = React.createClass({displayName: "Instagram",


    getInitialState: function () {
        return {'photos': []};

    },

    componentDidMount: function () {
        $.getJSON(endPoint, function (data) {
            this.setState({photos: data})
        }.bind(this));
    },
    render: function () {
        var renderPhoto = function (photo) {
            return React.createElement("div", null, 
            React.createElement("a", {href: photo.images.standard_resolution.url}, React.createElement("img", {src: photo.images.thumbnail.url})), 
            photo.caption.text
            )
        };
        return React.createElement("div", {className: "react-component "}, 
        this.state.photos.map(renderPhoto)
        );

    }

});


React.render(React.createElement(Instagram, null), document.getElementById('app'));
