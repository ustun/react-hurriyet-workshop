import React, { Component } from 'react';
import {Alert, Button} from 'react-bootstrap';
import _ from 'underscore';
import $ from 'jQuery';

require('purecss/build/pure.css');

require('./style.css');

import HelloWorld from './movie_app';

// var HelloWorld = function(props) {
//     return <div className="">
//       Hello {props.name} {props.surname}!
//     </div>;

//   }



var TodoApp = React.createClass({

  getInitialState() {
    return {
      temporaryTodo: "this is temp",
      filterType: "all",
      todos: [{description: "Patates al",
                  checked: false},
                 {description: "Corba yap",
                  checked: false},
                 {description: "Ehliyet al",
                  checked: true}]};
  },

  setFilterType(filterType) {
    this.setState({filterType: filterType});
  },

  toggleTodo(description) {
    var currentTodos = this.state.todos;

    for (var i = 0; i < currentTodos.length; i++) {
      if (currentTodos[i].description === description) {
        currentTodos[i].checked = !currentTodos[i].checked;
      }
    }

    this.setState({todos: currentTodos});

  },

    componentDidMount() {
      // this?
      setInterval(() => {
        // this?
        // this.toggleTodo("Patates al");
      }, 1000);
    },

  nItemsLeft() {
    return this.state.todos.filter(function (todo) {
      return !todo.checked;
    }).length;

  },

  setTemporaryTodo(e) {
    if (e.target.value.includes('1')) {
      this.setState({temporaryTodo: "Izin yok"})

    } else {
      this.setState({temporaryTodo: e.target.value})
    }
  },

  handleSubmit(e) {
    e.preventDefault();

    this.addTodo(this.state.temporaryTodo);

  },

  addTodo: function () {
    var currentTodos = this.state.todos;

    var newTodos = [{description: this.state.temporaryTodo, checked: false},
                    ...currentTodos];

    this.setState({todos: newTodos, temporaryTodo: ''});

  },

destroyTodo: function (description) {
    var currentTodos = this.state.todos;

    var newTodos = currentTodos.filter(function (todo) {
      return todo.description != description;
    });

    this.setState({todos: newTodos});

  },
  render() {

    var visibleTodos;

    if (this.state.filterType == "all") {
      visibleTodos = this.state.todos;
    }

    if (this.state.filterType == "active") {
      visibleTodos = this.state.todos.filter(function (todo) {
        return !todo.checked;
      });
    }

    if (this.state.filterType == "completed") {
      visibleTodos = this.state.todos.filter(function (todo) {
        return todo.checked;
      });
    }


    return <section id="todoapp">
  <header id="header">
	<h1>todos</h1>
      <form onSubmit={this.handleSubmit}>
	<input id="new-todo" value={this.state.temporaryTodo}
     onChange={this.setTemporaryTodo}
     placeholder="What needs to be done?" autofocus=""/>
      </form>

  {this.state.temporaryTodo}
  </header>
  <section id="main" style={{display: 'block'}}>
	<input id="toggle-all" type="checkbox"/>
	<label htmlFor="toggle-all">Mark all as complete</label>
	<ul id="todo-list">
{visibleTodos.map((todo, i) => {
  // debuggerugger
  console.log(i);
    return (
       <li key={todo.description}>
		<div className="view">
		  <input
           checked={todo.checked}
           onChange={()=>{this.toggleTodo(todo.description)}}
           className="toggle"
           type="checkbox"
          />
		  <label>{todo.description}</label>
		  <button className="destroy"
           onClick={()=> this.destroyTodo(todo.description)}></button>
		</div>
		<input className="edit" value="Buy tomatos"/>
	  </li>);
})}
</ul>
  </section>
  <footer id="footer" style={{display: 'block'}}>
	<span id="todo-count"><strong>{this.nItemsLeft()}</strong> items left</span>
	<ul id="filters">
                                {this.state.filterType}
	  <li>
		<a className={this.state.filterType == "all" && "selected"} href="#/"
         onClick={() => this.setFilterType("all")}>All</a>
	  </li>
	  <li>
		<a className={this.state.filterType == "active" && "selected"} href="#/active" onClick={() => this.setFilterType("active")}>Active</a>
	  </li>
	  <li>
		<a className={this.state.filterType == "completed" && "selected"} href="#/completed" onClick={() => this.setFilterType("completed")}>Completed</a>
	  </li>
	</ul>

  </footer>
</section>;

  }

});


export default HelloWorld;

// export default TodoApp;
