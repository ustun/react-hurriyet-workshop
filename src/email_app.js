var EmailContent = React.createClass({

    render() {
        var subject = "Hello from Ankara";
        return           <div className="email-content">
        <div className="email-content-header pure-g">
        <div className="pure-u-1-2">
        <h1 className="email-content-title">{subject}</h1>
        <p className="email-content-subtitle">
        From <a>Tilo Mitra</a> at <span>3:56pm, April 3, 2012</span>
        </p>
        </div>
        <div className="email-content-controls pure-u-1-2">
        <button className="secondary-button pure-button">Reply</button>
        <button className="secondary-button pure-button">Forward</button>
        <button className="secondary-button pure-button">Move to</button>
        </div>
        </div>
        <div className="email-content-body">
        <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                                                                                                                                                                                                                </p>
        <p>
        Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                                                                                                                                                                                            </p>
        <p>
        Aliquam ac feugiat dolor. Proin mattis massa sit amet enim iaculis tincidunt. Mauris tempor mi vitae sem aliquet pharetra. Fusce in dui purus, nec malesuada mauris. Curabitur ornare arcu quis mi blandit laoreet. Vivamus imperdiet fermentum mauris, ac posuere urna tempor at. Duis pellentesque justo ac sapien aliquet egestas. Morbi enim mi, porta eget ullamcorper at, pharetra id lorem.
                                                                                                                                                                                                                                                                                                                                                                                                          </p>
        <p>
        Donec sagittis dolor ut quam pharetra pretium varius in nibh. Suspendisse potenti. Donec imperdiet, velit vel adipiscing bibendum, leo eros tristique augue, eu rutrum lacus sapien vel quam. Nam orci arcu, luctus quis vestibulum ut, ullamcorper ut enim. Morbi semper erat quis orci aliquet condimentum. Nam interdum mauris sed massa dignissim rhoncus.
                                                                                                                                                                                                                                                                    </p>
        <p>
        Regards,<br />
        Tilo
        </p>
        </div>
        </div>;
    }

});

var EmailItem = React.createClass({

    render: function () {


        var email = this.props.email;
        var classes = "email-item pure-g";

        if (email.unread) {
            classes += " email-item-unread";
        }

        if (email.selected) {
            classes += " email-item-selected";
        }


        return <div className={classes}>
        <div className="pure-u">
        <img className="email-avatar" alt="Tilo Mitra's avatar" height={64} width={64} src={email.avatar} />
        </div>
        <div className="pure-u-3-4">
        <h5 className="email-name">{email.name}</h5>
        <h4 className="email-subject">{email.subject}</h4>
        <p className="email-desc">
        {email.description}
        </p>
        </div>
        </div>;
    }
})
var EmailList = React.createClass({

    render: function () {

        var email = {
            name: "Ustun Ozgur",
            subject: "Hello from Toronto111",
            description: "Hey, I just wanted to check in with you from Toronto. I got here earlier today.",
            avatar: "img/common/tilo-avatar.png",
            unread: true,
            selected: true
        };

        var emails = [email, email, email, email];

        return (
            <div id="list" className="pure-u-1">
            {emails.map((email, i) => <EmailItem i={i} email={email}/>)}
            </div>
        )
    }
})

var EmailApp = React.createClass({
    render: function() {

        return (

            <div id="layout" className="content pure-g">
            <div id="nav" className="pure-u">
            <a href="#" className="nav-menu-button">Menu</a>
            <div className="nav-inner">
            <button className="primary-button pure-button">Compose</button>
            <div className="pure-menu">
            <ul className="pure-menu-list">
            <li className="pure-menu-item"><a href="#" className="pure-menu-link">Inbox <span className="email-count">(2)</span></a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link">Important</a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link">Sent</a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link">Drafts</a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link">Trash</a></li>
            <li className="pure-menu-heading">Labels</li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-personal" />Personal</a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-work" />Work</a></li>
            <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-travel" />Travel</a></li>
            </ul>
            </div>
            </div>
            </div>
            <EmailList/>

            <div id="main" className="pure-u-1">
            <EmailContent/>
            </div>
            </div>
        );
    }
});

var App = React.createClass({

    render() {
        return <EmailApp/>;

    }

});
