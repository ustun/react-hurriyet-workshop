
var Urunler = React.createClass({
    render: function() {
        return (

            <div className="box-container loader" data-bind="css:{loading: reloadingResultsOnPageLoad}">
            <div className="contain-lg-4 contain-md-3 contain-sm-3 contain-xs-2 contain-xxs-1 fluid with-bottom-border">
            <ul className="product-list results-container  grid" data-bind="css: {grid: isGridSelected, list: isListSelected}">
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/apple-iphone-5s-16-gb-apple-turkiye-garantili-p-TELCEPIPH5S6GBSI-N" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_2871344.jpg" className="product-image owl-lazy" alt="Apple iPhone 5s 16 GB (Apple Türkiye Garantili)" title="Apple iPhone 5s 16 GB (Apple Türkiye Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_2871344.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(243)</span>
            </div>
            <h3 className="product-title title" title="Apple iPhone 5s 16 GB (Apple Türkiye Garantili)">
            <div>
            <p>
            Apple iPhone 5s 16 GB (Apple Türkiye Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>21</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">1.598,99 TL</del>
            <span className="price product-price">1.269,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/samsung-galaxy-note-5-32-gb-ithalatci-garantili-p-TELCEPSAMNTE5GRB32" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_11228633.jpg" className="product-image owl-lazy" alt="Samsung Galaxy Note 5 32 GB (İthalatçı Garantili)" title="Samsung Galaxy Note 5 32 GB (İthalatçı Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_11228633.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(55)</span>
            </div>
            <h3 className="product-title title" title="Samsung Galaxy Note 5 32 GB (İthalatçı Garantili)">
            <div>
            <p>
            Samsung Galaxy Note 5 32 GB (İthalatçı Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>24</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">2.499,00 TL</del>
            <span className="price product-price">1.899,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/lg-g3-d855-32gb-ithalatci-garantili-p-TELCEPLGG332-G" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_3868994.jpg" className="product-image owl-lazy" alt="LG G3 D855 32GB (İthalatçı Garantili)" title="LG G3 D855 32GB (İthalatçı Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_3868994.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(825)</span>
            </div>
            <h3 className="product-title title" title="LG G3 D855 32GB (İthalatçı Garantili)">
            <div>
            <p>
            LG G3 D855 32GB (İthalatçı Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>33</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">1.499,00 TL</del>
            <span className="price product-price">998,88 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/samsung-16gb-microsd-evo-class10-48mb-sn-hafiza-karti-sd-adaptor-mb-mp16da-tr-p-FTHFZSAMEVOMC16GB" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Camera/200/Camera_4325204.jpg" className="product-image owl-lazy" alt="Samsung 16GB MicroSD Evo Class10 48mb/sn Hafıza Kartı + SD Adaptör MB-MP16DA/TR" title="Samsung 16GB MicroSD Evo Class10 48mb/sn Hafıza Kartı + SD Adaptör MB-MP16DA/TR" src="//images.hepsiburada.net/assets/Camera/200/Camera_4325204.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(157)</span>
            </div>
            <h3 className="product-title title" title="Samsung 16GB MicroSD Evo Class10 48mb/sn Hafıza Kartı + SD Adaptör MB-MP16DA/TR">
            <div>
            <p>
            Samsung 16GB MicroSD Evo Class10 48mb/sn Hafıza Kartı + SD Adaptör MB-MP16DA/TR
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>71</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">45,01 TL</del>
            <span className="price product-price">12,90 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/apple-iphone-6s-16-gb-ithalatci-garantili-p-TELCEPIPH6S16GR-G" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_11909116.jpg" className="product-image owl-lazy" alt="Apple iPhone 6S 16 GB (İthalatçı Garantili)" title="Apple iPhone 6S 16 GB (İthalatçı Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_11909116.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(33)</span>
            </div>
            <h3 className="product-title title" title="Apple iPhone 6S 16 GB (İthalatçı Garantili)">
            <div>
            <p>
            Apple iPhone 6S 16 GB (İthalatçı Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>17</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">3.099,00 TL</del>
            <span className="price product-price">2.559,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/lg-g4-32-gb-dual-sim-ithalatci-garantili-p-TELCEPLGG432GR-DS3" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_5603281.jpg" className="product-image owl-lazy" alt="LG G4 32 GB Dual Sim (İthalatçı Garantili)" title="LG G4 32 GB Dual Sim (İthalatçı Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_5603281.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(706)</span>
            </div>
            <h3 className="product-title title" title="LG G4 32 GB Dual Sim (İthalatçı Garantili)">
            <div>
            <p>
            LG G4 32 GB Dual Sim (İthalatçı Garantili)
            </p>
            </div>
            </h3>
            <span className="info">
            <span className="info-text options">
            Bu ürün geçici olarak temin edilememektedir.
                                                        </span>
            </span>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/samsung-galaxy-j5-3g-dual-sim-ithalatci-garantili-p-TELCEPSAMJ5GR-A8" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_10675806.jpg" className="product-image owl-lazy" alt="Samsung Galaxy J5 3G Dual Sim (İthalatçı Garantili)" title="Samsung Galaxy J5 3G Dual Sim (İthalatçı Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_10675806.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '80%'}} />
            </span>
            <span className="number-of-reviews">(15)</span>
            </div>
            <h3 className="product-title title" title="Samsung Galaxy J5 3G Dual Sim (İthalatçı Garantili)">
            <div>
            <p>
            Samsung Galaxy J5 3G Dual Sim (İthalatçı Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>8</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">729,00 TL</del>
            <span className="price product-price">669,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/xiaomi-10400-mah-tasinabilir-sarj-cihazi-p-TELSARJXIAOMI10400" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_3835370.jpg" className="product-image owl-lazy" alt="Xiaomi 10400 mAh Taşınabilir Şarj Cihazı" title="Xiaomi 10400 mAh Taşınabilir Şarj Cihazı" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_3835370.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(485)</span>
            </div>
            <h3 className="product-title title" title="Xiaomi 10400 mAh Taşınabilir Şarj Cihazı">
            <div>
            <p>
            Xiaomi 10400 mAh Taşınabilir Şarj Cihazı
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>30</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">99,90 TL</del>
            <span className="price product-price">69,90 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/sandisk-32gb-microsd-48mb-s-class10-hafiza-karti-sdsqunb-032g-gn3mn-p-FTHFZSND32GMCG48" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Camera/200/Camera_5766305.jpg" className="product-image owl-lazy" alt="Sandisk 32GB MicroSD 48MB/s Class10 Hafıza Kartı SDSQUNB-032G-GN3MN" title="Sandisk 32GB MicroSD 48MB/s Class10 Hafıza Kartı SDSQUNB-032G-GN3MN" src="//images.hepsiburada.net/assets/Camera/200/Camera_5766305.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(123)</span>
            </div>
            <h3 className="product-title title" title="Sandisk 32GB MicroSD 48MB/s Class10 Hafıza Kartı SDSQUNB-032G-GN3MN">
            <div>
            <p>
            Sandisk 32GB MicroSD 48MB/s Class10 Hafıza Kartı SDSQUNB-032G-GN3MN
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>59</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">60,00 TL</del>
            <span className="price product-price">24,90 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/lenovo-a7000-p-TELCEPLENA7000-S" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_8665656.jpg" className="product-image owl-lazy" alt="Lenovo A7000" title="Lenovo A7000" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_8665656.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(88)</span>
            </div>
            <h3 className="product-title title" title="Lenovo A7000">
            <div>
            <p>
            Lenovo A7000
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>26</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">799,00 TL</del>
            <span className="price product-price">589,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/samsung-galaxy-a5-samsung-turkiye-garantili-p-TELCEPSAMA500HQ-B" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Telefon/200/Telefon_4251514.jpg" className="product-image owl-lazy" alt="Samsung Galaxy A5 (Samsung Türkiye Garantili)" title="Samsung Galaxy A5 (Samsung Türkiye Garantili)" src="//images.hepsiburada.net/assets/Telefon/200/Telefon_4251514.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '80%'}} />
            </span>
            <span className="number-of-reviews">(57)</span>
            </div>
            <h3 className="product-title title" title="Samsung Galaxy A5 (Samsung Türkiye Garantili)">
            <div>
            <p>
            Samsung Galaxy A5 (Samsung Türkiye Garantili)
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>34</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">1.499,00 TL</del>
            <span className="price product-price">989,01 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li className="search-item col lg-1 md-1 sm-1 xs-1 xxs-1 ">
            <div className="box product">
            <a href="/casio-mtp-1374l-1avdf-erkek-kol-saati-p-SAMTP-1374L-1AVDF" data-bind="click: clickHandler.bind($data)" data-rrclickurl>
            <figure className="product-image-wrapper">
            <img width={200} height={200} data-src="//images.hepsiburada.net/assets/Saat/200/Saat_4864782.jpg" className="product-image owl-lazy" alt="Casio Mtp-1374L-1Avdf Erkek Kol Saati" title="Casio Mtp-1374L-1Avdf Erkek Kol Saati" src="//images.hepsiburada.net/assets/Saat/200/Saat_4864782.jpg" />
            </figure>
            <div className="product-detail">
            <div className="ratings-container">
            <span className="ratings">
            <span className="ratings active" style={{width: '100%'}} />
            </span>
            <span className="number-of-reviews">(414)</span>
            </div>
            <h3 className="product-title title" title="Casio Mtp-1374L-1Avdf Erkek Kol Saati">
            <div>
            <p>
            Casio Mtp-1374L-1Avdf Erkek Kol Saati
            </p>
            </div>
            </h3>
            <div className="price-container highlight-badge">
            <div className="badge highlight discount-badge">
            <small>%</small><span>39</span>
            <span className="discount">
            indirim
            </span>
            </div>
            <del className="price old product-old-price">244,00 TL</del>
            <span className="price product-price">149,00 TL</span>
            </div>
            <div className="badge-container">
            <div className="badges">
            <div className="badge shipping-status">
            <span>SÜPER HIZLI</span>
            </div>
            </div>
            </div>
            </div>
            </a>
            </div>
            </li>
            <li id="reloadRoute" className="hidden" data-nextpage> </li>
            </ul>
            </div>
            </div>
        );
    }
});



var Sepet = React.createClass({

    render() {
        return <div className="">
        Sepet
        </div>;

    }

});
