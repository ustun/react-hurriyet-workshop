import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

var insan = {name: "Reyhan", surname: "Ustun"};
// ReactDOM.render(<App {...insan}/>, document.getElementById('root'));

ReactDOM.render(React.createElement(App, insan), document.getElementById('root'));
