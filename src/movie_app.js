import React, { Component } from 'react';
import {Alert, Button} from 'react-bootstrap';
import _ from 'underscore';
import $ from 'jQuery';


// https://api.themoviedb.org/3/movie/550?api_key=8c464df5d6537d2432f8d88af669137a
var postJSON = function (opts) {

    var defaultParams = {
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8'
    };

    var finalOpts = _.extend(
        defaultParams,
        opts,
        {data: JSON.stringify(opts.data)}
    );
    $.ajax(finalOpts);

};


var Movie = React.createClass({

    getInitialState: function () {
        return {detailedDataFetched: false, detailedDataShown: false, detailedData: null};
    },

    getMovieData: function () {
        if (!this.state.detailedDataFetched) {
            var [title, year] = this.props.movie.Title.split('(');
            year = year.split(')')[0]

            fetch(`http://www.omdbapi.com/?t=${title}&y=${year}&plot=short&r=json`).then(data=>data.json()).then(function (data) {
                this.setState({detailedDataFetched: true, detailedDataShown: true, detailedData: data});
            }.bind(this));

      getPosterPath(title, function (image) {
        this.setState({image: image});
      }.bind(this));
        }
    },

    toggleDetailedData() {

        if (this.state.detailedDataFetched) {
            this.setState({detailedDataShown: !this.state.detailedDataShown});
        } else {
            this.getMovieData();
        }

    },

    render() {
        var movie = this.props.movie;
        return <div className="">
        {movie.id} {movie.Rating} {movie.Title}
      {this.state.image && <img src={this.state.image} width={100}/>}
        <button className="pure-button" onClick={this.toggleDetailedData}>Toggle Detailed Data</button>
        {this.state.detailedDataShown && <pre>{JSON.stringify(this.state.detailedData, null, 4)}</pre>}
        </div>;

    }

});



var Movies = React.createClass({

    render() {
        var movies = this.props.movies;
        var renderMovie = function (movie, i) {
            return <Movie movie={movie} key={movie.id}/>;
        };

        /*         debugger */
        return <div className="">
        {movies.map(renderMovie)}
        </div>;

    }

});

var uid = 250;

var AddMovie = React.createClass({


    getInitialState() {
        return {title: "", year: "", rating: "", id: "", submitAttempted: false};
    },

    setTitle(e) {

        this.setState({title: e.target.value});

    },

    setYear(e) {

        this.setState({year: e.target.value});

    },

    setRating(e) {

        this.setState({rating: e.target.value});

    },

    setId(e) {

        this.setState({id: e.target.value});

    },


    validate() {
        return this.state.title && this.state.year && this.state.rating && this.state.id;
    },

    handleSubmit(e) {
        e.preventDefault();

        this.setState({submitAttempted: true});

        if (this.validate()) {
            this.submitData();
        } else {

        }
    },

    getData() {
        return {Rating: String(this.state.rating), Title: `${this.state.title} (${this.state.year})`, id: String(this.state.id)};
    },

    submitData() {
        postJSON({url: 'http://localhost:3004/movies',
                  data: this.getData(),
                  success: function (data) {
                      console.log("successfully posted", data);
                      this.setState(this.getInitialState());
                      this.props.onSuccessCallback();
                  }.bind(this),
                  error: function (e) {
                      console.error("not successfully posted", e);

                  }});
    },

    render() {
        return <div className="pure-u-1-5">
        <form className="pure-form pure-form-stacked" onSubmit={this.handleSubmit}>
        <h2>Add or Update Movie</h2>
        <input value={this.state.id} onChange={this.setId} placeholder="Id"/>

        <input value={this.state.title} onChange={this.setTitle} placeholder="Title"/>
        <input value={this.state.year} onChange={this.setYear} placeholder="Year"/>
        <input value={this.state.rating} onChange={this.setRating} placeholder="Rating"/>
        {this.state.submitAttempted && !this.validate() && <div className="alert">Form is invalid</div>}
        <button className="pure-button pure-button-primary" type="submit">Submit</button>
        </form>
        </div>
    }

});


var MoviesStateful = React.createClass({
    getInitialState() {
        return {movies: [], moviesFetched: false, moviesFilter: ""};
    },

    componentWillMount() {

        this.fetchMovies();

    },

    fetchMovies() {
        fetch("http://localhost:3004/movies").then(data=>data.json()).then(data => this.setState({movies: data, moviesFetched: true}));
    },

    setMoviesFilter(e) {
        this.setState({moviesFilter: e.target.value});
    },

    componentDidMount() {
        setTimeout(()=> this.refs.search.focus(), 1000);
    },

    render() {
        if (this.state.moviesFetched) {
            var shownMovies = this.state.moviesFilter ?
                              this.state.movies.filter(movie => movie.Title.toLowerCase().includes(this.state.moviesFilter.toLowerCase())) :
                              this.state.movies;
            return (
                <div className="pure-g">

                <div className="pure-u-4-5">
                <h2>Movie List</h2>
                <form className="pure-form" onSubmit={e => e.preventDefault()}>
                <input className="pure-input" ref="search" placeholder="Search for a movie" value={this.state.moviesFilter} onChange={this.setMoviesFilter}/>
                <button className="pure-button pure-button-primary">Search</button>
                </form>
                <Movies movies={shownMovies}/>
                </div>
                <AddMovie onSuccessCallback={this.fetchMovies}/>
                </div>);
        } else {
            return <div>Fetching data</div>;
        }

    }

});

var fetchAndLog = function (url) {
    fetch(url).then(function (data) { return data.json();}).then(function (data) { console.log(data);})
}

var k = "8c464df5d6537d2432f8d88af669137a";
fetchAndLog(`https://api.themoviedb.org/3/movie/550?api_key=${k}`);

var posterPath = "https://image.tmdb.org/t/p/w370/x8rbmk1wJL6eDEA8AlMGsbpLbxK.jpg";



var getPosterPath = function (movieName, cb) {
    var api = `http://api.themoviedb.org/3/search/movie?query=${encodeURI(movieName)}&api_key=${k}`;
    fetch(api).then(data=>data.json()).then(function (data) {
        var posterPath = `https://image.tmdb.org/t/p/w370/${data.results[0].poster_path}`;
        console.log("poster path is", posterPath);
        cb && cb(posterPath);
    });
};

getPosterPath("shawshank");

fetch("http://www.omdbapi.com/?t=The+Shawshank+Redemption&y=1994&plot=short&r=json").then(function (data) { return data.json();}).then(function (data) { console.log(data);})
export default MoviesStateful;
